/**
 * 
 */
package com.myindo.project.pengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class HasilKaryawanModel {
	private String total_cuti;

	public String getTotal_cuti() {
		return total_cuti;
	}

	public void setTotal_cuti(String total_cuti) {
		this.total_cuti = total_cuti;
	}
}
