/**
 * 
 */
package com.myindo.project.pengajuancuti.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String dateOfPaidLeave;
	private String totalDays;
	private String reason;
	private String dateOfEntry;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getDateOfPaidLeave() {
		return dateOfPaidLeave;
	}
	public void setDateOfPaidLeave(String dateOfPaidLeave) {
		this.dateOfPaidLeave = dateOfPaidLeave;
	}
	public String getTotalDays() {
		return totalDays;
	}
	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
}
