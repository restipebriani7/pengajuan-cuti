/*
 * 
 */
package com.myindo.project.pengajuancuti.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;
import java.util.logging.Logger;

import com.myindo.project.pengajuancuti.connection.Connect;
import com.myindo.project.pengajuancuti.model.HasilKaryawanModel;
import com.myindo.project.pengajuancuti.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class InsertPengajuanCuti {
	Logger log = Logger.getLogger("InsertPengajuanCuti");
	String codeResponse = "";
	String idString;
	

	public String addCuti(RequestModel rModel) throws Exception {
		Connection con = Connect.connection();
		
		HasilKaryawanModel hModel = new HasilKaryawanModel();
		System.out.println("Npk : " + rModel.getNpk());

		try {
			//autoincreatment id
			Random random =  new Random();
			int id = random.nextInt(100000);
			String idString = Integer.toString(id);
	        
			PreparedStatement pStm = con.prepareStatement("SELECT * FROM karyawan WHERE npk=?");
			pStm.setString(1, rModel.getNpk());
			ResultSet rSet = pStm.executeQuery();

			while (rSet.next()) {
				hModel.setTotal_cuti(rSet.getString("total_cuti"));
			}
			rSet.close();
			log.info("Select total_cuti dari tb karyawan berhasil");
			System.out.println("Total Cuti : " + hModel.getTotal_cuti());
			
			System.out.println("Request jumlah cuti : "+rModel.getTotalDays());
			
			int sisa_cuti = Integer.parseInt(hModel.getTotal_cuti());
			int reJumCuti = Integer.parseInt(rModel.getTotalDays());
			int jmlCt = sisa_cuti - reJumCuti;
			String jmlString = Integer.toString(jmlCt);
			System.out.println("Sisa Cuti : "+jmlCt);

			if (jmlCt < 0) {
				codeResponse = "1111";
				log.info("Total cuti anda tidak mencukupi untuk pengajuan cuti");
			} else {
				PreparedStatement rr = con
						.prepareStatement("INSERT INTO pengajuan_cuti (id_pengajuan, tgl_cuti, jml_hari, ket_cuti, tgl_masuk, npk) VALUES (?, ?::date, ?::int, ?, ?::date, ?)");

				rr.setString(1, idString);
				rr.setString(2, rModel.getDateOfPaidLeave());
				rr.setString(3, rModel.getTotalDays());
				rr.setString(4, rModel.getReason());
				rr.setString(5, rModel.getDateOfEntry());
				rr.setString(6, rModel.getNpk());

				rr.executeUpdate();			
				rr.close();
				codeResponse = "0000";
				log.info("Pengajuan cuti berhasil");
			}
			if (codeResponse.equals("0000")) {
				PreparedStatement ps = con.prepareStatement("UPDATE karyawan SET total_cuti=? WHERE npk=?");
				ps.setString(1, jmlString);
				ps.setString(2, rModel.getNpk());
				ps.executeUpdate();
				
				log.info("Update total cuti ke tabel karyawan berhasil");
			}
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Pengajuan cuti Failed");
			log.info(e.getMessage());
		}
		return codeResponse;
	}
}
